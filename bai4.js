/**
 * INPUT
 * 
 * nhập vào lần lược chiều dài và rộng của hình chủ nhật
 * chiều dài là 4
 * chiều rộng là 6
 * 
 * TODO
 * 
 * Công thức chu vi là; ( dài + rộng)*2
 *  Công thức tính diện tích là ; dài*rộng
 * 
 * OUTPUT
 * 
 * chu vi là 20
 * diện tích là 24
 * 
 */
function daiRong(){
    var chieuDai= document.getElementById("txt-dai").value*1;
    var chieuRong= document.getElementById("txt-rong").value*1;
    var chuVi= (chieuDai+chieuRong)*2;
    var dienTich= chieuDai*chieuRong;
    document.getElementById("dientich").innerText= `Diện tích hình là : ${dienTich}`;
    document.getElementById("chuvi").innerText= `Chu vu hình là : ${chuVi}`;
}
